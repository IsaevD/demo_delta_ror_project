class CreateServices < ActiveRecord::Migration
  def change
    create_table :services do |t|
      t.string :alias
      t.string :name
      t.text :short_description
      t.text :long_description

      t.timestamps
    end
  end
end
