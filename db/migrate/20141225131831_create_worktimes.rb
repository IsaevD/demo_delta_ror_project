class CreateWorktimes < ActiveRecord::Migration
  def change
    create_table :worktimes do |t|
      t.string :alias
      t.string :name
      t.string :value

      t.timestamps
    end
  end
end
