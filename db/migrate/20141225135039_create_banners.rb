class CreateBanners < ActiveRecord::Migration
  def change
    create_table :banners do |t|
      t.integer :position
      t.has_attached_file :image

      t.timestamps
    end
  end
end
