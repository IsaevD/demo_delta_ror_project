class CreatePrices < ActiveRecord::Migration
  def change
    create_table :prices do |t|
      t.string :name
      t.string :cost
      t.integer :service_id
      t.integer :parent_id

      t.timestamps
    end
  end
end
