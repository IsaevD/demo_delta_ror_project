class CreateProducts < ActiveRecord::Migration
  def change
    create_table :products do |t|
      t.string :name
      t.integer :service_id
      t.has_attached_file :image

      t.timestamps
    end
  end
end
