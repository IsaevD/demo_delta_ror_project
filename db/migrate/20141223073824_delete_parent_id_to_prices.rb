class DeleteParentIdToPrices < ActiveRecord::Migration
  def change
    remove_column :prices, :parent_id
  end
end
