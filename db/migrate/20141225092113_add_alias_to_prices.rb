class AddAliasToPrices < ActiveRecord::Migration
  def change
    add_column :prices, :alias, :string
  end
end
