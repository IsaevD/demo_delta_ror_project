class CreateReviews < ActiveRecord::Migration
  def change
    create_table :reviews do |t|
      t.string :name
      t.string :email
      t.text :review_text
      t.has_attached_file :image
      t.boolean :published

      t.timestamps
    end
  end
end
