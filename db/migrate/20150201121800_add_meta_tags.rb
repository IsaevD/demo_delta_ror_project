class AddMetaTags < ActiveRecord::Migration
  def change
    add_column :services, :title, :text
    add_column :services, :metakeywords, :text
    add_column :services, :metadescription, :text
  end
end
