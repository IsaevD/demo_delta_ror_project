class CreateAdvices < ActiveRecord::Migration
  def change
    create_table :advices do |t|
      t.string :name
      t.text :description
      t.integer :service_id

      t.timestamps
    end
  end
end
