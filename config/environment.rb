# Load the Rails application.
require File.expand_path('../application', __FILE__)

# Initialize the Rails application.
Delta::Application.initialize!

ActionMailer::Base.smtp_settings = {
    :user_name => 'noreply.delta18@gmail.com',
    :password => 'delta2015',
    :domain => 'gmail.com',
    :address => 'smtp.gmail.com',
    :port => 587,
    :authentication => :plain,
    :enable_starttls_auto => true
}