Delta::Application.routes.draw do

  constraints subdomain: 'www' do
    get ':any', to: redirect(subdomain: nil, path: '/%{any}'), any: /.*/
  end

  root 'index#index'
  match '/services/:alias', to: 'services#show', via: 'get'
  match '/contacts', to: 'contacts#index', via: 'get'
  resources :reviews, :only => [:index, :create]
  resources :calls, :only => [:create]
  resources :orders, :only => [:create]

  namespace :admin do

    resources :sessions, only: [:new, :create, :destroy]
    root 'sessions#new'
    get '/login' => 'sessions#new'
    match '/logout', to: 'sessions#destroy', via: "delete"

    resources :users
    resources :services
    resources :settings
    resources :prices
    resources :advices
    resources :products
    resources :reviews
    resources :worktimes
    resources :banners
    resources :calls, only: [:index, :destroy]
    resources :orders
  end

  get '*anything', :controller => 'index', :action  => 'e404'

end
