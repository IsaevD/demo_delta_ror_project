module WorktimesHelper
  def get_current_worktime
    worktime_value = Worktime.find_by(:alias => 'Special').value
    if worktime_value.empty?
      days = %w{ Sun Mon Tue Wed Thu Fri Sat }
      day_name = days[Time.now.strftime("%w").to_i]
      worktime_value = Worktime.find_by(:alias => day_name)
      if !worktime_value.nil?
        worktime_value = worktime_value.value
      end
    end
    worktime_value
  end
end
