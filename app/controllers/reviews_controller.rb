require 'net/http'
require 'json'

class ReviewsController < ApplicationController

  def index
    @reviews = Review.where(:published => true)
  end

  def create
    @review = Review.new(reviews_params)
    uri = URI.parse("https://www.google.com/recaptcha/api/siteverify?secret=6Ld2sAETAAAAADeTTQ_lctaSGbckvTdXrDRJxHZr&response="+params['g-recaptcha-response'])
    response = Net::HTTP.get_response(uri)
    content = response.body
    h = JSON.parse content
    if (h["success"].to_s == "true")
      if @review.save
        MainMailer.review_mail(@review).deliver
      end
    end
    render :text => h["success"]
  end

  private

    def reviews_params
      params.require(:review).permit(:name, :email, :review_text, :image, :published)
    end

end