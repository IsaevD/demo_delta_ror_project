require 'net/http'
require 'json'
class OrdersController < ApplicationController

  def create
    @order = Order.new(order_params)
    uri = URI.parse("https://www.google.com/recaptcha/api/siteverify?secret=6LeOrgETAAAAADLDAsZ970EbRi1gOsZFahpASRFM&response="+params['g-recaptcha-response'])
    response = Net::HTTP.get_response(uri)
    content = response.body
    h = JSON.parse content
    if (h["success"].to_s == "true")
      if @order.save
        MainMailer.order_mail(@order).deliver
      end
    end
    render :text => h["success"]
  end

  private

    def order_params
      params.require(:order).permit(:name, :note, :phone, :service_id)
    end

end
