class IndexController < ApplicationController

  def index
    @reviews = Review.where(:published => true)
    @banners = Banner.all.order("position ASC")
  end

  def e404
    render :status => '404'
  end

end
