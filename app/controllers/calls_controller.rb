require 'net/http'
require 'json'
class CallsController < ApplicationController

  def create
    @call = Call.new(call_params)
    uri = URI.parse("https://www.google.com/recaptcha/api/siteverify?secret=6LdtrQETAAAAABXZCGNyMDkIc6sopLGWVja-_wy3&response="+params['g-recaptcha-response'])
    response = Net::HTTP.get_response(uri)
    content = response.body
    h = JSON.parse content
    if (h["success"].to_s == "true")
      if @call.save
        MainMailer.call_mail(@call).deliver
      end
    end
    render :text => h["success"]
  end

  private

    def call_params
      params.require(:call).permit(:name, :phone)
    end

end
