class ServicesController < ApplicationController
  def show
    @service = Service.find_by(:alias => params[:alias])
    if !@service.nil?
      @children_services = Service.where(:parent_id => @service.id).order("created_at ASC")
      @reviews = Review.where(:published => true)
    else
      render "index/e404", :status => '404'
    end
  end
end