class Admin::SessionsController < ApplicationController

  layout "admin"

  def new
    if signed_in?
      redirect_to admin_services_path
    end
  end

  def create
    @user = User.find_by_name(params[:session][:name])
    if @user && @user.authenticate(params[:session][:password])
      sign_in @user
      redirect_to admin_services_path
    else
      render 'new'
    end
  end

  def destroy
    sign_out
    redirect_to admin_login_path
  end

end
