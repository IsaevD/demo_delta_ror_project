class Admin::SettingsController < ApplicationController

  before_filter :signed_in_user
  layout "admin"

  def index
    @settings = Setting.all.order("created_at ASC")
  end

  def show
    @setting = Setting.find(params[:id])
  end

  def new
    @setting = Setting.new
  end

  def create
    @setting = Setting.new(setting_params)
    if @setting.save
      redirect_to admin_settings_path
    else
      render 'new'
    end
  end

  def edit
    @setting = Setting.find(params[:id])
  end

  def update
    @setting = Setting.find(params[:id])
    if @setting.update_attributes(setting_params)
      redirect_to admin_settings_path
    else
      render 'edit'
    end
  end

  def destroy
    Setting.find(params[:id]).destroy
    redirect_to admin_settings_path
  end

  private

  def setting_params
    params.require(:setting).permit(:alias, :name, :value)
  end

end
