class Admin::PricesController < ApplicationController

  before_filter :signed_in_user
  layout "admin"

  def index
    f_service_id = params[:service]
    if !f_service_id.nil?
      f_service_id = f_service_id[:f_service_id]
    end
    if f_service_id.nil?
      @prices = Price.all
    else
      if f_service_id.empty?
        @prices = Price.all
      else
        @prices = Price.where(:service_id => f_service_id)
      end
    end


  end

  def show
    @price = Price.find(params[:id])
  end

  def new
    @price = Price.new
  end

  def create
    @price = Price.new(service_params)
    if @price.save
      redirect_to admin_prices_path
    else
      render 'new'
    end
  end

  def edit
    @price = Price.find(params[:id])
  end

  def update
    @price = Price.find(params[:id])
    if @price.update_attributes(service_params)
      redirect_to admin_prices_path
    else
      render 'edit'
    end
  end

  def destroy
    Price.find(params[:id]).destroy
    redirect_to admin_prices_path
  end

  private

    def service_params
      params.require(:price).permit(:alias, :pos_type, :name, :cost, :service_id, :parent_id)
    end

end
