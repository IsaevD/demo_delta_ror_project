class Admin::AdvicesController < ApplicationController

  before_filter :signed_in_user
  layout "admin"

  def index
    @advices = Advice.all
  end

  def show
    @advice = Advice.find(params[:id])
  end

  def new
    @advice = Advice.new
  end

  def create
    @advice = Advice.new(advice_params)
    if @advice.save
      redirect_to admin_advices_path
    else
      render 'new'
    end
  end

  def edit
    @advice = Advice.find(params[:id])
  end

  def update
    @advice = Advice.find(params[:id])
    if @advice.update_attributes(advice_params)
      redirect_to admin_advices_path
    else
      render 'edit'
    end
  end

  def destroy
    Advice.find(params[:id]).destroy
    redirect_to admin_advices_path
  end

  private

  def advice_params
    params.require(:advice).permit(:name, :description, :service_id)
  end

end
