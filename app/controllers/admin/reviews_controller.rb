class Admin::ReviewsController < ApplicationController

  before_filter :signed_in_user
  layout "admin"

  def index
    @reviews = Review.all
  end

  def show
    @review = Review.find(params[:id])
  end

  def new
    @review = Review.new
  end

  def create
    @review = Review.new(reviews_params)
    if @review.save
      redirect_to admin_reviews_path
    else
      render 'new'
    end
  end

  def edit
    @review = Review.find(params[:id])
  end

  def update
    @review = Review.find(params[:id])
    if @review.update_attributes(reviews_params)
      redirect_to admin_reviews_path
    else
      render 'edit'
    end
  end

  def destroy
    Review.find(params[:id]).destroy
    redirect_to admin_reviews_path
  end

  private

    def reviews_params
      params.require(:review).permit(:name, :email, :review_text, :image, :published)
    end

end
