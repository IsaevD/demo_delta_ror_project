class Admin::ServicesController < ApplicationController

  before_filter :signed_in_user
  layout "admin"

  def index
    @services = Service.all
  end

  def show
    @service = Service.find(params[:id])
  end

  def new
    @service = Service.new
  end

  def create
    @service = Service.new(service_params)
    if @service.save
      redirect_to admin_services_path
    else
      render 'new'
    end
  end

  def edit
    @service = Service.find(params[:id])
  end

  def update
    @service = Service.find(params[:id])
    if @service.update_attributes(service_params)
      redirect_to admin_services_path
    else
      render 'edit'
    end
  end

  def destroy
    Service.find(params[:id]).destroy
    redirect_to admin_services_path
  end

  private

    def service_params
      params.require(:service).permit(:alias, :name, :short_description, :long_description, :title, :metadescription, :metakeywords, :parent_id)
    end

end
