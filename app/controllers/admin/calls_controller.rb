class Admin::CallsController < ApplicationController

  before_filter :signed_in_user
  layout "admin"

  def index
    @calls = Call.all.order("created_at DESC")
  end

  def destroy
    Call.find(params[:id]).destroy
    redirect_to admin_calls_path
  end

end
