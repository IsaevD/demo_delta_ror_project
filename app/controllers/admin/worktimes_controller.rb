class Admin::WorktimesController < ApplicationController

  before_filter :signed_in_user
  layout "admin"

  def index
    @worktimes = Worktime.where("alias <> 'Special'").order("created_at ASC")
    @week = {"Mon" => "Понедельник", "Tue" => "Вторник", "Wed" => "Среда", "Thu" => "Четверг", "Fri" => "Пятница", "Sat" => "Суббота", "Sun" => "Воскресенье", "Special" => "Исключение"}
    #[['Понедельник', 'Mon'], ['Вторник', 'Tue'], ['Среда', 'Wed'], ['Четверг', 'Thu'], ['Пятница', 'Fri'], ['Суббота', 'Sat'], ['Воскресенье', 'Sun'], ['Особенный день', 'Special']]
  end

  def show
    @worktime = Worktime.find(params[:id])
  end

  def new
    @worktime = Worktime.new
  end

  def create
    @worktime = Worktime.new(worktime_params)
    if @worktime.save
      redirect_to admin_worktimes_path
    else
      render 'new'
    end
  end

  def edit
    @worktime = Worktime.find(params[:id])
    @week = {"Mon" => "Понедельник", "Tue" => "Вторник", "Wed" => "Среда", "Thu" => "Четверг", "Fri" => "Пятница", "Sat" => "Суббота", "Sun" => "Воскресенье", "Special" => "Исключение"}
  end

  def update
    @worktime = Worktime.find(params[:id])
    if @worktime.update_attributes(worktime_params)
      redirect_to admin_worktimes_path
    else
      render 'edit'
    end
  end

  def destroy
    Worktime.find(params[:id]).destroy
    redirect_to admin_worktimes_path
  end

  private

  def worktime_params
    params.require(:worktime).permit(:alias, :name, :value)
    end

end
