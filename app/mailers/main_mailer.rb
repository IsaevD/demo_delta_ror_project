class MainMailer < ActionMailer::Base
  default from: "noreply.delta18@gmail.com"
  default to: "deltasrv@yandex.ru"

  def review_mail(review)
    @review = review
    mail(subject: 'Новый отзыв на сайте delta-18.ru')
  end

  def order_mail(order)
    @order = order
    mail(subject: 'Новая заявка на сайте delta-18.ru')
  end

  def call_mail(call)
    @call = call
    mail(subject: 'Новая звонок на сайте delta-18.ru')
  end

end
