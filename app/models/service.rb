class Service < ActiveRecord::Base
  has_many :prices
  has_many :advices
  has_many :products
  has_many :orders
end
