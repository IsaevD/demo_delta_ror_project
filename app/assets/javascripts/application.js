// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/sstephenson/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery_ujs
// require turbolinks
// require_tree .

var sliderTimer;
// Период переключени главного слайдера
var sliderPeriodInSecond = 5;
$(function(){
    initialServicesChange();
    initialCallPopup();
    initialClosePopup();
    initialReviewPopup();
    initialOrderPopup();
    initialCallSubmit();
    initialReviewSubmit();
    initialOrderSubmit();
    initialSlider();
    initialReviewsSlider();
    initialAdvicesSlider();
    initialProductsSlider();


});

$(window).load(function(){
    initialRecaptcha();
});

function initialRecaptcha() {
    //Render the recaptcha1 on the element with ID "recaptcha1"
    recaptcha1 = grecaptcha.render('r1', {
        'sitekey': "6Ld2sAETAAAAACs5GGOKul7ikGsHqAeRIKq_AawI", //Replace this with your Site key
        'theme': 'light'
    });

    recaptcha2 = grecaptcha.render('r2', {
        'sitekey': "6LdtrQETAAAAANTOmvVP2u34KmH57tpmrmyPgDqy", //Replace this with your Site key
        'theme': 'light'
    });

    recaptcha3 = grecaptcha.render('r3', {
        'sitekey': "6LeOrgETAAAAAPFl6fQwb0ZclHZcDtyaUGdl-cdU", //Replace this with your Site key
        'theme': 'light'
    });
}

function initialServicesChange() {
    $("#prices_service_id").change(function(){
        $("#price-img .prices.enabled").removeClass("enabled")
        $("#price-img .prices-tbl.enabled").removeClass("enabled")
        pricesClass = $(this).val();
        $(".gadget").attr("src", "/assets/images/panels/" + $(this).val() + ".png");
        $("#price-img").children(".prices." + pricesClass).addClass("enabled");
        $("#price-img").children(".prices-tbl." + pricesClass).addClass("enabled");
    });
}

function initialCallPopup() {
    $("#call-order").click(function(){
        changeBGstate(1);
        changeFORMstate("#new_call", 1);
    });
}

function initialReviewPopup() {
    $("#add-review").click(function(){
        changeBGstate(1);
        changeFORMstate("#new_review", 1);
    });
}

function initialOrderPopup() {
    $("#order-btn").click(function(){
        changeBGstate(1);
        changeFORMstate("#new_order", 1);
    });
}

function initialClosePopup() {
    $(".close-popup ").click(function(){
        $("#new_call .inputs").show();
        $("#new_call .description").hide();
        $("#new_review .inputs").show();
        $("#new_review .description").hide();
        $("#new_order .inputs").show();
        $("#new_order .description").hide();
        $("#popup-forms").hide();
        $("#new_call").hide();
        $("#new_review").hide();
        $("#new_order").hide();
        changeBGstate(0);
    });
}

function changeBGstate(state) {
    if (state == 1)
        $("#popup-bg").show();
    else
        $("#popup-bg").hide();
}

function changeFORMstate(form_id, state) {
    if (state == 1) {
        $("#popup-forms").show();
        $(form_id).show();
    }
    else {
        $(form_id).hide();
        $("#popup-forms").hide();
    }
}

function initialCallSubmit() {
    $("#new_call").submit(function(){
        $("#new_call .error").hide();
        error = false;
        if ($("#new_call #call_name").val() == "") {
            $("#new_call #call_name").next().show();
            error = true;
        }
        if ($("#new_call #call_phone").val() == "") {
            $("#new_call #call_phone").next().show();
            error = true;
        }
        if (!error) {
            postData = $(this).serializeArray();
            $.ajax({
                type: "POST",
                data: postData,
                url: $(this).attr("action"),
                success: function (data) {
                    if (data == "true") {
                        $("#new_call .inputs").hide();
                        $("#new_call .description").show();
                    } else {
                        $("#new_call .gcaptcha").next().show();
                    }
                }
            });
        }
        return false;
    });
}

function initialReviewSubmit() {
    $("#new_review").submit(function(){
        $("#new_review .error").hide();
        error = false;
        if ($("#new_review #review_name").val() == "") {
            $("#new_review #review_name").next().show();
            error = true;
        }
        if ($("#new_review #review_review_text").val() == "") {
            $("#new_review #review_review_text").next().show();
            error = true;
        }
        if (!error) {
            postData = $(this).serializeArray();
            $.ajax({
                type: "POST",
                data: postData,
                url: $(this).attr("action"),
                success: function (data) {
                    if (data == "true") {
                        $("#new_review .inputs").hide();
                        $("#new_review .description").show();
                    } else {
                        $("#new_review .gcaptcha").next().show();
                    }
                }
            });
        }
        return false;
    });
}


function initialOrderSubmit() {
    $("#new_order").submit(function(){
        $("#new_order .error").hide();
        error = false;
        if ($("#new_order #order_name").val() == "") {
            $("#new_order #order_name").next().show();
            error = true;
        }
        if ($("#new_order #order_note").val() == "") {
            $("#new_order #order_note").next().show();
            error = true;
        }
        if ($("#new_order #order_phone").val() == "") {
            $("#new_order #order_phone").next().show();
            error = true;
        }
        if (!error) {
            postData = $(this).serializeArray();
            $.ajax({
                type: "POST",
                data: postData,
                url: $(this).attr("action"),
                success: function (data ) {
                    if (data == "true") {
                        $("#new_order .inputs").hide();
                        $("#new_order .description").show();
                    } else {
                        $("#new_order .gcaptcha").next().show();
                    }

                }
            });
        }
        return false;
    });
}

// Инициализация слайдера
function initialSlider() {
    $('#slider-cnt').width($('#slider-cnt').children().size() * $('#slider').width());
    $('#slider-cnt').children('li.slider-list').width($('#slider').width());
    $(window).resize(function () {
        $('#slider-cnt').width($('#slider-cnt').children().size() * $('#slider').width());
        $('#slider-cnt').children('li.slider-list').width($('#slider').width());
    });
    sliderTimer = setInterval(nextSlide, sliderPeriodInSecond*1000);
    $('#slider').hover(
        function(){
            clearInterval(sliderTimer);
        },
        function(){
            sliderTimer = setInterval(nextSlide, sliderPeriodInSecond*1000);
        });
    buttonSlide();
}

function nextSlide() {
    var currentSlide = parseInt($('#slider-cnt').data('current'));
    $($("#slider-menu").children("li")[currentSlide]).removeClass("active");
    currentSlide++;
    if(currentSlide>=$('#slider').children().size()) {
        currentSlide = 0;
    }
    $($("#slider-menu").children("li")[currentSlide]).addClass("active");
    changeSlide(currentSlide);
}

function changeSlide(index) {
    $('#slider-cnt').
        animate({left: -index*$('#slider').width()},300).data('current', index);
}

// Переключение слайда по нажатию кнопки
function buttonSlide() {
    $("#slider-menu li").click(function (element) {
        if ($(element.target).attr("class") != "active") {
            $("#slider-menu li.active").removeClass("active");
            $(element.target).addClass("active");
            changeSlide($(element.target).index());
        }
        return false;
    });
}

function initialReviewsSlider() {
    $('#reviews-cnt ul').width($('#reviews-cnt ul').children().size() * $('#reviews-cnt').width());
    $('#reviews-cnt ul').children('li').width($('#reviews-cnt').width());
    $(window).resize(function () {
        $('#reviews-cnt ul').width($('#reviews-cnt ul').children().size() * $('#reviews-cnt').width());
        $('#reviews-cnt ul').children('li').width($('#reviews-cnt').width());
    });
    nextReview();
    prevReview();
}

function nextReview() {
    $("#reviews .right-arrow").click(function(){
        var currentSlide = parseInt($('#reviews-cnt ul').data('current'));
        currentSlide++;
        if(currentSlide>=$('#reviews-cnt ul').children().size()) {
            currentSlide = 0;
        }
        changeReview(currentSlide);
    });
}

function prevReview() {
    $("#reviews .left-arrow").click(function(){
        var currentSlide = parseInt($('#reviews-cnt ul').data('current'));
        currentSlide--;
        if(currentSlide<0) {
            currentSlide = $('#reviews-cnt ul').children().size()-1;
        }
        changeRightReview(currentSlide);
    });
}

function changeReview(index) {
    $('#reviews-cnt ul').
        animate({left: -index*$('#reviews-cnt').width()},300).data('current', index);
}
function changeRightReview(index) {
    $('#reviews-cnt ul').
        animate({left: -index*$('#reviews-cnt').width()},300).data('current', index);
}



function initialAdvicesSlider() {
    $('#advices ul').width($('#advices ul').children().size() * $('#advices').width());
    $('#advices ul').children('li').width($('#advices').width());
    $(window).resize(function () {
        $('#advices ul').width($('#advices ul').children().size() * $('#advices').width());
        $('#advices ul').children('li').width($('#advices').width());
    });

    nextAdvice();
    prevAdvice();

}

function nextAdvice() {
    $("#service-advices .right-arrow").click(function(){
        var currentSlide = parseInt($('#advices ul').data('current'));
        currentSlide++;
        if(currentSlide>=$('#advices ul').children().size()) {
            currentSlide = 0;
        }
        changeLeftAdvice(currentSlide);
    });
}

function prevAdvice() {
    $("#service-advices .left-arrow").click(function(){
        var currentSlide = parseInt($('#advices ul').data('current'));
        currentSlide--;
        if(currentSlide<0) {
            currentSlide = $('#advices ul').children().size()-1;
        }
        changeRightAdvice(currentSlide);
    });
}

function changeLeftAdvice(index) {
    $('#advices ul').
        animate({left: -index*$('#advices').width()},300).data('current', index);
}
function changeRightAdvice(index) {
    $('#advices ul').
        animate({left: -index*$('#advices').width()},300).data('current', index);
}


function initialProductsSlider() {
    $('#products ul').width($('#products ul').children().size() * $('#products').width());
  //  $('#products ul').children('li').width($('#products').width() * 0.20);
  //  $('#products .white-left').width(($('#products li').first().width() + 20) * 0.9);
  //  $('#products .white-right').width(($('#products li').first().width() + 20) * 0.9);
        $(window).resize(function () {
        $('#products ul').width($('#products ul').children().size() * $('#products').width());
    //    $('#products ul').children('li').width($('#products').width() * 0.20);
    //    $('#products .white-left').width(($('#products li').first().width() + 20) * 0.9);
    //    $('#products .white-right').width(($('#products li').first().width() + 20) * 0.9);
    });
    nextProduct();
    prevProduct();
}

function nextProduct() {
    $("#recommended .right-arrow").click(function(){
        var currentSlide = parseInt($('#products ul').data('current'));
        currentSlide++;
        if(currentSlide>=$('#products ul').children().size()) {
            currentSlide = 0;
        }
        changeLeftProduct(currentSlide);
    });
}

function prevProduct() {
    $("#recommended .left-arrow").click(function(){
        var currentSlide = parseInt($('#products ul').data('current'));
        currentSlide--;
        if(currentSlide<0) {
            currentSlide = $('#products ul').children().size()-1;
        }
        changeRightProduct(currentSlide);
    });
}

function changeLeftProduct(index) {
    $('#products ul').
        animate({left: -index*($('#products li').first().width()+20)},300).data('current', index);
}
function changeRightProduct(index) {
    $('#products ul').
        animate({left: -index*($('#products li').first().width()+20)},300).data('current', index);
}
